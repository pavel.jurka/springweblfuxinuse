package cz.creativedock.techtalk.content.client

import cz.creativedock.techtalk.content.client.contract.KeyValue
import reactor.core.publisher.Mono


interface ContentClient {
    fun findValue(key: String): Mono<KeyValue>
    fun storeValue(data: KeyValue): Mono<KeyValue>
    fun updateValue(data: KeyValue): Mono<KeyValue>
}