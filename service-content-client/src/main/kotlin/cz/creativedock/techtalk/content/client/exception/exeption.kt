package cz.creativedock.techtalk.content.client.exception

class NotFoundException (what: String):  kotlin.Exception("$what was not found")

class ConnectionException (reason: String):  kotlin.Exception("Unable to connect - $reason")