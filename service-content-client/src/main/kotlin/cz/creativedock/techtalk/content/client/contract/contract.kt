package cz.creativedock.techtalk.content.client.contract

data class KeyValue(val key: String, val value: String)