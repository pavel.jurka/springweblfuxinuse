package cz.creativedock.techtalk.content.client.impl

import cz.creativedock.techtalk.content.client.ContentClient
import cz.creativedock.techtalk.content.client.contract.KeyValue
import cz.creativedock.techtalk.content.client.exception.ConnectionException
import cz.creativedock.techtalk.content.client.exception.NotFoundException
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientResponseException
import reactor.core.publisher.Mono
import java.lang.Exception

@Component
class ContentClientImpl: ContentClient {

    private val logger = LogManager.getLogger(ContentClientImpl::class.java)

    @Autowired
    lateinit var contentWebClient: WebClient

    override fun findValue(key: String): Mono<KeyValue> {
        logger.debug("Find Value called with $key")

        return contentWebClient
                .get()
                .uri("/data/$key")
                .retrieve()
                .bodyToMono(KeyValue::class.java)
                .doOnError { e -> logger.error("Unable to get value for $key", e) }
                .doOnSuccess { resp -> logger.debug("$resp") }
                .onErrorMap { t -> mapToError(t) }
    }

    override fun storeValue(data: KeyValue): Mono<KeyValue> {
        logger.debug("Store called with $data")

        return contentWebClient
                .post()
                .uri("/save")
                .body(BodyInserters.fromObject(data))
                .retrieve()
                .bodyToMono(KeyValue::class.java)
                .doOnError { e -> logger.error("Unable to store value for $data", e) }
                .doOnSuccess { resp -> logger.debug("$resp") }
                .onErrorMap { t -> mapToError(t) }
    }

    override fun updateValue(data: KeyValue): Mono<KeyValue> {
        logger.debug("Update called with $data")

        return contentWebClient
                .post()
                .uri("/update")
                .body(BodyInserters.fromObject(data))
                .retrieve()
                .bodyToMono(KeyValue::class.java)
                .doOnError { e -> logger.error("Unable to store value for $data", e) }
                .doOnSuccess { resp -> logger.debug("$resp") }
                .onErrorMap { t -> mapToError(t) }
    }

    private fun mapToError(t: Throwable?): Throwable? {

        return if (t is WebClientResponseException) {

            val e = t as WebClientResponseException

            when (e.rawStatusCode) {
                HttpStatus.NOT_FOUND.value() -> NotFoundException(e.responseBodyAsString)
                else -> Exception()
            }
        } else {
            ConnectionException(t?.message.let { it } ?: "unknown")
        }
    }



}
