plugins {
    kotlin("jvm")
    idea
}

dependencies {

    compile("org.apache.logging.log4j:log4j-api:2.12.0")

    // JSON serialization
    compile("com.fasterxml.jackson.core:jackson-databind:2.9.6")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.9")


    compile("org.springframework:spring-webflux:5.1.6.RELEASE")
    compile("org.springframework:spring-context:5.1.6.RELEASE")

    compile("org.springframework.cloud:spring-cloud-sleuth-zipkin:2.1.2.RELEASE")

}