import groovy.lang.GroovyObject
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    base
    kotlin("jvm") version "1.3.21" apply false
    java
    `maven-publish`
}

allprojects {

    group = "cz.creativedock.techtalk"

    repositories {
        jcenter()
        mavenLocal()
        maven { setUrl("https://repo.maven.apache.org/maven2") }
        maven { setUrl("https://repo.spring.io/snapshot") }
        maven { setUrl("https://repo.spring.io/milestone") }
        maven { setUrl("http://oss.jfrog.org/artifactory/oss-snapshot-local/") }
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
}

subprojects {
    if (name.startsWith("s")) {
        apply(plugin = "maven")
        apply(plugin = "maven-publish")
    }
}
val sourcesJar by tasks.registering(Jar::class) {
    from(sourceSets.main.get().allSource)
}


dependencies {
    // Make the root project archives configuration depend on every sub-project
    subprojects.forEach {
        archives(it)
    }
}