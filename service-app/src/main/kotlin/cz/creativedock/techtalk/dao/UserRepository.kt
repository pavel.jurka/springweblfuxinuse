package cz.creativedock.techtalk.dao

import cz.creativedock.techtalk.model.User
import cz.creativedock.techtalk.model.UserInfo
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: CrudRepository<User, Long> {
    fun getUserByUsername(username: String): User?

    fun getById(id: Long): User?
}