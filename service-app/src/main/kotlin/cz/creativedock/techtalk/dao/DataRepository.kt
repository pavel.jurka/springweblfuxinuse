package cz.creativedock.techtalk.dao

import cz.creativedock.techtalk.model.DataEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface DataRepository: CrudRepository<DataEntity, Long>