package cz.creativedock.techtalk.security.service

import com.beust.klaxon.Klaxon
import cz.creativedock.techtalk.model.UserInfo
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

@Service
class JWTService {

    @Value("\${app.security.jjwt.secret}")
    lateinit var secret: String

    @Value("\${app.security.jjwt.expiration}")
    lateinit var expirationTime: String

    fun getAllClaimsFromToken(token: String): Claims {
        return Jwts.parser().setSigningKey(Base64.getEncoder()
                .encodeToString(secret.toByteArray()))
                .parseClaimsJws(token)
                .body
    }

    fun getUsernameFromToken(token: String): String {
        return getAllClaimsFromToken(token).subject
    }

    fun getExpirationDateFromToken(token: String): Date {
        return getAllClaimsFromToken(token).expiration
    }

    private fun isTokenExpired(token: String) = getExpirationDateFromToken(token).before(Date())

    fun generateToken(userInfo: UserInfo): String {
        val claims = HashMap<String, Any>()
        claims["role"] = listOf(userInfo.role)
        return doGenerateToken(claims, userInfo)
    }

    private fun doGenerateToken(claims: Map<String, Any>, userInfo: UserInfo): String {
        val expirationTimeLong = java.lang.Long.parseLong(expirationTime) //in second
        val createdDate = Date()
        val expirationDate = Date(createdDate.time + expirationTimeLong * 1000000)
        val key = Keys.secretKeyFor(SignatureAlgorithm.HS512)
        val userInfoAsString: String = Klaxon().toJsonString(userInfo)

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userInfoAsString)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(key)
                .signWith(SignatureAlgorithm.HS512, Base64.getEncoder().encodeToString(secret.toByteArray()))
                .compact()
    }

    fun validateToken(token: String): Boolean = !isTokenExpired(token)

}