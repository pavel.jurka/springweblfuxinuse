package cz.creativedock.techtalk.security.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.security.NoSuchAlgorithmException
import java.security.spec.InvalidKeySpecException
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

@Service
class PBKDF2EncoderService : PasswordEncoder {

    @Value("\${app.security.password.encoder.secret}")
    lateinit var secret: String

    @Value("\${app.security.password.encoder.iteration}")
    var iteration = 0

    @Value("\${app.security.password.encoder.keylength}")
    var keylength = 0

    /**
     * More info (https://www.owasp.org/index.php/Hashing_Java)
     * @param cs password
     * @return encoded password.
     */
    override fun encode(cs: CharSequence): String {
        try {
            val result = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
                    .generateSecret(PBEKeySpec(cs.toString().toCharArray(), secret.toByteArray(), iteration, keylength))
                    .encoded
            return Base64.getEncoder().encodeToString(result);
        } catch (e: Exception) {
            e.multicatch(NoSuchAlgorithmException::class, InvalidKeySpecException::class) {
                throw RuntimeException(e)
            }
        }
    }


    override fun matches(rawPassword: CharSequence, encodedPassword: String): Boolean {
        return encode(rawPassword) == encodedPassword
    }


    fun <R> Throwable.multicatch(vararg classes: KClass<*>, block: () -> R): R {
        if (classes.any { this::class.isSubclassOf(it) }) {
            return block()
        } else throw this
    }
}
