package cz.creativedock.techtalk.security


import cz.creativedock.techtalk.security.service.AuthManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain
import reactor.core.publisher.Mono

@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class WebSecurityConfig {
    @Autowired
    lateinit var authenticateManager: AuthManager

    @Autowired
    lateinit var securityContextRepository: SecurityContextRepository

    @Bean
    fun securityWebFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        return http
                .exceptionHandling()
                .authenticationEntryPoint { swe, _ -> Mono.fromRunnable<Void> { swe.response.statusCode = HttpStatus.UNAUTHORIZED } }
                .accessDeniedHandler { swe, _ -> Mono.fromRunnable<Void> { swe.response.statusCode = HttpStatus.FORBIDDEN } }.and()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .authenticationManager(authenticateManager)
                .securityContextRepository(securityContextRepository)
                .authorizeExchange()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .pathMatchers("/actuator/health").permitAll()
                .pathMatchers("/actuator/info").permitAll()
                .pathMatchers("/actuator/prometheus").permitAll()

                .pathMatchers("/data/update").permitAll()
                .pathMatchers("/data/store").permitAll()
                .pathMatchers("/data/key1").permitAll()
                .pathMatchers("/test/rollback1").permitAll()
                .pathMatchers("/test/rollback2").permitAll()
                .pathMatchers("/test/rollback3").permitAll()

                .pathMatchers("/login-user").permitAll()
                .pathMatchers("/login-admin").permitAll()




                .anyExchange().authenticated()
                .and().build()
    }
}