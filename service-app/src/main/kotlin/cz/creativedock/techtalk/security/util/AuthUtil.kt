package cz.creativedock.techtalk.security.util

import com.beust.klaxon.Klaxon
import cz.creativedock.techtalk.exception.ValidationException
import cz.creativedock.techtalk.model.UserInfo
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import reactor.core.publisher.Mono

object AuthUtil {
    fun getUser(): Mono<UserInfo> {
        return ReactiveSecurityContextHolder.getContext()
                .map { context -> context.authentication }
                .map { authentication ->
                    val principal = authentication.principal
                    if (principal is String) {
                        val userInfo = Klaxon().parse<UserInfo>(principal)
                        userInfo?.let { it } ?: throw ValidationException("user not found")
                    } else {
                        throw ValidationException("invalid principal")
                    }
                }
    }
}
