package cz.creativedock.techtalk.security.service.impl


import cz.creativedock.techtalk.security.service.UserService
import cz.creativedock.techtalk.content.client.exception.NotFoundException
import cz.creativedock.techtalk.dao.UserRepository
import cz.creativedock.techtalk.model.User
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl: UserService {

    private val log = LoggerFactory.getLogger(UserServiceImpl::class.java.name)

    @Autowired
    lateinit var repository: UserRepository

    override fun getUserByUsername(username: String): User {

        log.debug("Get user by $username")

        return  repository.getUserByUsername(username) ?: throw NotFoundException("Unable to find user by $username")
    }

}