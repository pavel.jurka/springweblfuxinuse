package cz.creativedock.techtalk.security.service

import cz.creativedock.techtalk.model.Role
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono


@Component
class AuthManager : ReactiveAuthenticationManager {


    @Autowired
    lateinit var jwtUtil: JWTService

    override fun authenticate(authentication: Authentication): Mono<Authentication> {
        val authToken = authentication.credentials.toString()

        var username: String?
        try {
            username = jwtUtil.getUsernameFromToken(authToken)
        } catch (e: Exception) {
            username =
                    null
        }

        return if (username != null && jwtUtil.validateToken(authToken)) {
            val claims = jwtUtil.getAllClaimsFromToken(authToken)
            @Suppress("UNCHECKED_CAST")
            val rolesMap: List<String> = claims.get("role", List::class.java) as List<String>
            val roles = java.util.ArrayList<Role>()
            for (rolemap in rolesMap) {
                roles.add(Role.valueOf(rolemap))
            }
            val auth = UsernamePasswordAuthenticationToken(
                    username, null,
                    roles.map { authority -> SimpleGrantedAuthority(authority.name) }
            )
            Mono.just(auth)
        } else {
            Mono.empty()
        }
    }

}