package cz.creativedock.techtalk.security.service

import cz.creativedock.techtalk.model.User


interface UserService {
    fun getUserByUsername(username: String): User
}