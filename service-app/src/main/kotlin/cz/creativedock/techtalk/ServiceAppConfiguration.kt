package cz.creativedock.techtalk

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["cz.creativedock"])
class ServiceAppConfiguration