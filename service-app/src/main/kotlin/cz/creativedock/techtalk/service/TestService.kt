package cz.creativedock.techtalk.service

import cz.creativedock.techtalk.content.client.contract.KeyValue
import reactor.core.publisher.Mono

interface TestService {

    fun findValue(key: String): Mono<KeyValue>
    fun storeValue(data: KeyValue): Mono<KeyValue>
    fun updateValue(data: KeyValue): Mono<KeyValue>

    fun rollback1(): Mono<String>
    fun rollback2(): Mono<String>
    fun rollback3(): String


}