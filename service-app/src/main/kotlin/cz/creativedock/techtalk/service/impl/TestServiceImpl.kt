package cz.creativedock.techtalk.service.impl

import cz.creativedock.techtalk.content.client.ContentClient
import cz.creativedock.techtalk.content.client.contract.KeyValue
import cz.creativedock.techtalk.dao.DataRepository
import cz.creativedock.techtalk.model.DataEntity
import cz.creativedock.techtalk.service.TestService
import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct
import javax.transaction.Transactional

@Service
@Transactional
class TestServiceImpl: TestService{

    private val log = LoggerFactory.getLogger(TestServiceImpl::class.java.name)

    lateinit var counterRollback3: Counter

    @Autowired
    lateinit var registry: MeterRegistry

    @Autowired
    lateinit var contentClient: ContentClient

    @Autowired
    lateinit var dataRepository: DataRepository

    @PostConstruct
    fun init() {
        counterRollback3 = Counter
                .builder("count_rollback3")
                .description("rollback3")
                .register(registry)
    }

    override fun findValue(key: String): Mono<KeyValue> {
        return contentClient.findValue(key)
                .doOnError { log.error("Unable to find $key") }
    }


    override fun storeValue(data: KeyValue): Mono<KeyValue> {
        return contentClient.storeValue(data)
    }

    override fun updateValue(data: KeyValue): Mono<KeyValue> {
        return contentClient.updateValue(data)
    }


    override fun rollback1(): Mono<String> {

        val data = DataEntity(0L, "To be kept")

        dataRepository.save(data)

        return contentClient.updateValue(KeyValue("Not found", "xxx"))
                .map{""}
    }

    override fun rollback2(): Mono<String> {
        return Mono.just(sync())
    }

    private fun sync(): String {
        val data = DataEntity(0L, "Will be removed")
        dataRepository.save(data)
        contentClient.updateValue(KeyValue("Not found", "xxx")).block()
        return ""
    }

    override fun rollback3(): String {
        sync()
        counterRollback3.increment()
        return ""
    }



}