package cz.creativedock.techtalk.controller

import cz.creativedock.techtalk.security.service.JWTService
import cz.creativedock.techtalk.content.client.contract.KeyValue
import cz.creativedock.techtalk.model.Role
import cz.creativedock.techtalk.model.UserInfo
import cz.creativedock.techtalk.security.util.AuthUtil
import cz.creativedock.techtalk.service.TestService
import io.micrometer.core.annotation.Timed
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers

@RestController
class AppController {

    private val log = LoggerFactory.getLogger(AppController::class.java.name)

    @Autowired
    lateinit var jwtUtil: JWTService

    @Autowired
    lateinit var testService: TestService

    @ApiOperation(value = "Get user profile.")
    @GetMapping("/data/{key}")
    fun getValue(@PathVariable key: String): Mono<ResponseEntity<KeyValue>> {

        return testService.storeValue(KeyValue("key", "value"))
                .map { ResponseEntity.ok(it) }
                .doOnSuccess { log.debug("Found $key") }
                .doOnError { log.error("Failed $key", it) }
    }

    @ApiOperation(value = "Save KeyValue.")
    @PostMapping("/data/store")
    fun storeValue(@RequestBody data: KeyValue): Mono<ResponseEntity<KeyValue>> {

        return testService.storeValue(data)
                .map { ResponseEntity.ok(it) }
                .doOnSuccess { log.debug("Stored $data") }
                .doOnError { log.error("Failed to store $data", it) }
    }

    @ApiOperation(value = "Save KeyValue.")
    @PostMapping("/data/update")
    fun updateValue(@RequestBody data: KeyValue): Mono<ResponseEntity<KeyValue>> {

        return testService.updateValue(data)
                .map { ResponseEntity.ok(it) }
                .doOnSuccess { log.debug("Stored $data") }
                .doOnError { log.error("Failed to store $data", it) }
    }

    @GetMapping("/test/rollback1")
    fun rollback1(): Mono<ResponseEntity<String>> {

        return testService.rollback1()
                .map { ResponseEntity.ok(it) }
    }

    @GetMapping("/test/rollback2")
    fun rollback2(): Mono<ResponseEntity<String>> {

        return testService.rollback2()
                .map { ResponseEntity.ok(it) }

    }

    @GetMapping("/test/rollback3")
    @Timed("rollback3")
    fun rollback3(): Mono<ResponseEntity<String>> {

        return Mono.fromCallable { testService.rollback3() }
                .subscribeOn(Schedulers.elastic())
                .map { ResponseEntity.ok(it) }

    }

    @ApiOperation(value = "Login / init login to app.")
    @GetMapping("/login-user")
    fun loginInit(): Mono<ResponseEntity<String>> {


        val token = jwtUtil.generateToken(UserInfo(2L, username = "user", role = Role.ROLE_USER))

        return Mono.just(createLoginResponse(token))


    }

    private fun createLoginResponse(token: String): ResponseEntity<String> {
        return ResponseEntity
                .ok()
                .header(HttpHeaders.AUTHORIZATION, token)
                .header("Access-Control-Expose-Headers", "authorization")
                .body("")
    }

    @RequestMapping(value = ["/user"], method = [RequestMethod.GET])
    @PreAuthorize("hasRole('USER')")
    fun user(): Mono<ResponseEntity<*>> {
        return Mono.just(ResponseEntity.ok<Any>("Content for user"))
    }

    @RequestMapping(value = ["/auth"], method = [RequestMethod.GET])
    fun auth(): Mono<ResponseEntity<*>> {

        return AuthUtil.getUser()
                .map {
                    log.debug(it.username)
                    it.username
                }
                .map { ResponseEntity.ok(it) }
    }
}