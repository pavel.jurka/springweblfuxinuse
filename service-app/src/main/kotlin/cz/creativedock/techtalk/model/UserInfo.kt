package cz.creativedock.techtalk.model

import javax.persistence.*

data class UserInfo(val id: Long, val username: String, val role: Role)

@Entity
@Table(name = "al_user")
@SequenceGenerator(name = "al_user_id_seq_gen", sequenceName = "al_user_id_seq", allocationSize = 1)
data class User(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "al_user_id_seq_gen")
        var id: Long = 0L,

        @Column
        var username: String = "",

        @Column
        var password: String = "",

        @Column
        @Enumerated(EnumType.STRING)
        var role: Role = Role.ROLE_UNKNOWN

)