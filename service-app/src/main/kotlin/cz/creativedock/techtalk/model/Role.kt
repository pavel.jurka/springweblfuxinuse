package cz.creativedock.techtalk.model

enum class Role{
    ROLE_USER, ROLE_ADMIN, ROLE_UNKNOWN
}