package cz.creativedock.techtalk.model

import cz.creativedock.techtalk.content.client.contract.KeyValue
import javax.persistence.*

@Entity
@Table(name = "data")
@SequenceGenerator(name = "data_id_seq_gen", sequenceName = "data_id_seq", allocationSize = 1)
data class DataEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "data_id_seq_gen")
        var id: Long = 0L,

        @Column
        var data: String = ""
)