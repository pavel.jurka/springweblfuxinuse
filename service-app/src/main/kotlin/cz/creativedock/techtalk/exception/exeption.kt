package cz.creativedock.techtalk.exception

class ValidationException (error: String):  kotlin.Exception("$error is not valid")