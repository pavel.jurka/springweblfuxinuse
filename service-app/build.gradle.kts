import org.springframework.boot.gradle.tasks.bundling.BootJar

buildscript {
    val springBootVersion = "2.1.4.RELEASE"

    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:$springBootVersion")
    }
}

plugins {
    application
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.spring") version "1.3.41"
}

apply(plugin = "org.springframework.boot")
apply(plugin = "io.spring.dependency-management")


application {
    mainClassName = "cli.Main"
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

configurations {
    "compile" {
        exclude(module = "logback-classic")
        exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    }
}

dependencies {

    compile(project(":service-content-client"))

    compile("org.springframework.boot:spring-boot-starter-webflux")
    compile("org.springframework.boot:spring-boot-starter-actuator")
    compile("org.springframework.boot:spring-boot-starter-reactor-netty")
    compile("org.springframework.boot:spring-boot-starter-security")
    compile("org.springframework.boot:spring-boot-starter-log4j2")

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")

    runtimeOnly("org.postgresql:postgresql")
    implementation("org.liquibase:liquibase-core:3.7.0")

    compile("com.beust:klaxon:5.0.2")
    implementation("com.google.code.gson:gson:2.8.5")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7")


    implementation("com.beust:klaxon:5.0.2")


    // ZIPKIN
    compile("org.springframework.cloud:spring-cloud-sleuth-zipkin:2.1.2.RELEASE")

    //metrics
    compile("io.micrometer:micrometer-core:1.2.0")
    compile("io.micrometer:micrometer-registry-prometheus:1.2.0")
    compile("org.aspectj:aspectjweaver:1.9.4")

    //Swagger
    compile("io.springfox:springfox-swagger2:3.0.0-SNAPSHOT")
    compile("io.springfox:springfox-swagger-ui:3.0.0-SNAPSHOT")
    compile("io.springfox:springfox-spring-webflux:3.0.0-SNAPSHOT")


    //JJWT
    compile("io.jsonwebtoken:jjwt-api:0.10.7")
    runtimeOnly("io.jsonwebtoken:jjwt-impl:0.10.7")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:0.10.7")
}
