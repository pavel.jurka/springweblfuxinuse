package cz.creativedock.techtalk.content.service

import cz.creativedock.techtalk.content.client.contract.KeyValue
import reactor.core.publisher.Mono

interface KeyValueService {
    fun getKey(key: String): Mono<KeyValue>
    fun storeKey(data: KeyValue): Mono<KeyValue>
    fun update(data: KeyValue): Mono<KeyValue>
}