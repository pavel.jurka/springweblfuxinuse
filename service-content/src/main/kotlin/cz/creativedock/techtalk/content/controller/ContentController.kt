package cz.creativedock.techtalk.content.controller

import cz.creativedock.techtalk.content.service.KeyValueService
import cz.creativedock.techtalk.content.client.contract.KeyValue

import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
class ContentController {

    private val log = LoggerFactory.getLogger(ContentController::class.java.name)

    @Autowired
    lateinit var keyValueService: KeyValueService

    @ApiOperation(value = "Get user profile.")
    @GetMapping("/data/{key}")
    fun getValue(@PathVariable key: String): Mono<ResponseEntity<KeyValue>> {

        return keyValueService.getKey(key)
                .map{ResponseEntity.ok(it)}
                .doOnSuccess { log.debug("Found $key") }
                .doOnError { log.error("Failed $key", it) }
    }

    @ApiOperation(value = "Save data.")
    @PostMapping("/save")
    fun saveValue(@RequestBody data: KeyValue): Mono<ResponseEntity<KeyValue>> {

        return keyValueService.storeKey(data)
                .map{ResponseEntity.ok(it)}
                .doOnSuccess { log.debug("Saved $data") }
                .doOnError { log.error("Failed to save $data", it) }
    }

    @ApiOperation(value = "Update data.")
    @PostMapping("/update")
    fun updateValue(@RequestBody data: KeyValue): Mono<ResponseEntity<KeyValue>> {

        return keyValueService.update(data)
                .map{ResponseEntity.ok(it)}
                .doOnSuccess { log.debug("Saved $data") }
                .doOnError { log.error("Failed to save $data", it) }
    }

}