package cz.creativedock.techtalk.content.service.impl

import cz.creativedock.techtalk.content.service.KeyValueService
import cz.creativedock.techtalk.content.client.contract.KeyValue
import cz.creativedock.techtalk.content.client.exception.NotFoundException
import cz.creativedock.techtalk.content.dao.DataRepository
import cz.creativedock.techtalk.content.model.DataEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.sleuth.annotation.NewSpan
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class KeyValueServiceImpl : KeyValueService {

    @Autowired
    lateinit var dataRepository: DataRepository

    @NewSpan("getKey")
    override fun getKey(key: String): Mono<KeyValue> {
        return find(key)
    }

    private fun find(key: String): Mono<KeyValue> {
        return dataRepository.findByKey(key)
                ?.let { Mono.just(it.asKeyValue()) }
                ?: Mono.error(NotFoundException(key))
    }

    override fun storeKey(data: KeyValue): Mono<KeyValue> {

        val dataEntity = DataEntity(data)
        dataRepository.save(dataEntity)
        return Mono.just(dataEntity.asKeyValue())
    }

    override fun update(data: KeyValue): Mono<KeyValue> {

        return dataRepository.findByKey(data.key)
                ?.let{updateValue(it, data)}
                ?:Mono.error(NotFoundException(data.key))
    }

    private fun updateValue(entity: DataEntity, data: KeyValue): Mono<KeyValue> {
        entity.value = data.value
        dataRepository.save(entity)
        return Mono.just(entity.asKeyValue())
    }

}