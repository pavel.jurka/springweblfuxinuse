package cz.creativedock.techtalk.content.dao

import cz.creativedock.techtalk.content.model.DataEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface DataRepository: CrudRepository<DataEntity, Long> {
    fun findByKey(key: String): DataEntity?
}