package cz.creativedock.techtalk.content

import org.springframework.boot.SpringApplication
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.reactive.config.EnableWebFlux

@SpringBootApplication
@EnableWebFlux
class ContentApp {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val app = SpringApplication(ContentApp::class.java)
            app.webApplicationType = WebApplicationType.REACTIVE
            app.run(*args)
        }
    }
}