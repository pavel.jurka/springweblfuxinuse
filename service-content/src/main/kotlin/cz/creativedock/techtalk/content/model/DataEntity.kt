package cz.creativedock.techtalk.content.model

import cz.creativedock.techtalk.content.client.contract.KeyValue
import javax.persistence.*

@Entity
@Table(name = "data")
@SequenceGenerator(name = "data_id_seq_gen", sequenceName = "data_id_seq", allocationSize = 1)
data class DataEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "data_id_seq_gen")
        var id: Long = 0L,

        @Column
        var key: String = "",

        @Column
        var value: String = ""
) {
    constructor(data: KeyValue) : this(0L, data.key, data.value)

    fun asKeyValue(): KeyValue {
        return KeyValue(key, value)
    }
}
